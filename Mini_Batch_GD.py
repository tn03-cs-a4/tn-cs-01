import pandas as pd
import numpy as np


class Mat_Factorization():
  def __init__(self,data,batch_size=32,feature_count=5,epochs=50,learning_rate=0.1,lamb=10):
    self.data=data
    self.batch_size=batch_size
    self.feature_count=feature_count
    self.epochs=epochs
    self.learning_rate=learning_rate
    self.lamb=lamb
    
    self.user_count = data.shape[0]
    self.item_count = data.shape[1]
    #iterations is no of batches per epoch
    self.no_of_iters=int(self.user_count/self.batch_size)

    #initializing user_feature and item_feature matrices
    self.user_features = np.random.uniform(low=0.1,high=0.9,size=(self.user_count,self.feature_count))
    self.item_features= np.random.uniform(low=0.1,high=0.9,size=(self.item_count,self.feature_count))
  
  def rmse(self):
    predicted_mat = np.matmul(self.user_features,np.transpose(self.item_features))
    return np.sqrt(np.sum((self.data - predicted_mat)**2)/self.user_count)

  def update_if_mat(self):
    for j in range(0,self.item_count):
      #idy is tuple of all user_ids(only from current batch) who have rated item with item_id j(i.e. rating value is not 0)
      idy=np.nonzero(self.curr_batch[:,j])[0]
       #user_feat_row contains rows from user_feature matrix belonging to idy
      user_feat_row=self.user_features[idy][:]
      #item_feat_row is item_feature row corresponding to current item j
      item_feat_row=(self.item_features[j][:]).reshape(1,self.feature_count)
      actual_ratings=(self.curr_batch[idy,j]).reshape(len(idy),1)
      #the calculation below is based on formula 1 and formula 3
      error=np.matmul(np.transpose((np.matmul(user_feat_row,np.transpose(item_feat_row)))-actual_ratings),user_feat_row)
      self.item_features[j][:]=self.item_features[j][:]-self.learning_rate*(error+self.lamb*((item_feat_row)))
      # print("----------------------------------------------------------------------------------------------------")
      # print(self.item_features)
      # print("----------------------------------------------------------------------------------------------------")

  def update_uf_mat(self):
    for j in range(0,self.batch_size):
       #idx is tuple of all item_ids which have been rated by usr j in current batch
      idx=np.nonzero(self.curr_batch[j,:])[0]
      user_feat_row=(self.user_features[j][:]).reshape(1,self.feature_count) #Theta_temp
      item_feat=self.item_features[idx][:]#X_tem
      actual_ratings=(self.curr_batch[j][idx]).reshape(1,len(idx))#Y_tem
      #the calculation below is based on formula 2 and formula 3
      error=(np.matmul((np.transpose(np.matmul(item_feat,(np.transpose(user_feat_row))))-actual_ratings),item_feat))
      self.user_features[self.batch_start_index:self.batch_end_index][:]-=self.learning_rate*(error+self.lamb*((self.user_features[j][:]).reshape(1,self.feature_count)))

  def train_model(self):
    for k in range(1,self.epochs+1):
      self.batch_start_index=0
      for i in range(1,self.no_of_iters+1):
        self.batch_end_index=i*self.batch_size
        self.curr_batch=self.data[self.batch_start_index:self.batch_end_index][:]
        self.batch_start_index+=self.batch_size
        self.update_if_mat();
        self.update_uf_mat();
      print("uf")
      print(self.user_features)
      print("if")
      print(self.item_features)
      print("Epoch {} completed- RMSE- {}".format(k,self.rmse()))



#ref=pd.read_csv('/content/gdrive/My Drive/Utility_Mat_Zero.csv')
ref=pd.read_csv('./Utility_Mat_Zero.csv')
#selecting only 8000 uers and 1000 items for now
ref=ref.iloc[0:8000,0:1000]

ref=ref.values

mat_fact = Mat_Factorization(ref)
mat_fact.train_model()
##the rmse we are getting here is very large-
#Output:
#Epoch 1 completed- RMSE- 4239.310348905528
#Epoch 2 completed- RMSE- 643375859737.9747
#Epoch 3 completed- RMSE- 9.289222821372467e+36
#Epoch 4 completed- RMSE- 1.1073556937790088e+38
#Epoch 5 completed- RMSE- 4.4876573240276976e+38


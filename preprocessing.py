import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

data= pd.read_excel("./ratings.xlsx")

users=data['cust_id'].unique()
products=data['item_id'].unique()

user_index={}
def utility_mat(data):
  
  userList = data['cust_id'].tolist()
  productList = data['item_id'].tolist()
  ratingList =data['rating'].tolist()

  users=data['cust_id'].unique()
  products=data['item_id'].unique()

  for i in range(len(users)):
    user_index[users[i]]=i

  pd_dict = {product: [np.nan for i in range(len(users))] for product in products}

  for i in range(0,len(data)):
    product = productList[i]
    user = userList[i]
    rating = ratingList[i]

    pd_dict[product][user_index[user]]=rating

  X = pd.DataFrame(pd_dict)
  X.index = users
    

  return X

utility_matrix = utility_mat(data)

utility_matrix.to_csv("./Utility_Mat_NaN.csv")
Utility_Matrix_Zero = utility_matrix.replace(np.NaN,0)
Utility_Matrix_Zero.to_csv("./Utility_Mat_Zero.csv")
# Replace NaN by average
for i in utility_matrix.columns:
  sum_rating = utility_matrix[i].sum()
  non_zero_item = utility_matrix[i].count()
  average_rating = (sum_rating/non_zero_item)
  utility_matrix[i].replace(np.NaN,average_rating.round(1),inplace=True)
utility_matrix.to_csv("./Utility_Mat_Average.csv")

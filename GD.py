import pandas as pd
import numpy as np
import pickle


class Mat_Factorization():
  def __init__(self,data,batch_size=32,feature_count=5,epochs=50,learning_rate=0.1,lamb=10):
    self.data=data
    self.batch_size=batch_size
    self.feature_count=feature_count
    self.epochs=epochs
    self.learning_rate=learning_rate
    self.lamb=lamb
    
    self.user_count = data.shape[0]
    self.item_count = data.shape[1]

    self.user_features = np.random.uniform(low=0.1,high=0.9,size=(self.user_count,self.feature_count))
    self.item_features= np.random.uniform(low=0.1,high=0.9,size=(self.item_count,self.feature_count))
    self.predicted_mat=None 

  def rmse(self):
    self.predicted_mat = np.matmul(self.user_features,np.transpose(self.item_features))
    return np.sqrt(np.sum((self.data - self.predicted_mat)**2)/self.user_count)

  def update_if_mat(self):
    for j in range(0,self.item_count):
      #idy is tuple of all user_ids(only from current batch) who have rated item with item_id j(i.e. rating value is not 0)
      idy=np.nonzero(self.data[:,j])[0]
       #user_feat_row contains rows from user_feature matrix belonging to idy
      user_feat_row=self.user_features[idy][:]
      #item_feat_row is item_feature row corresponding to current item j
      item_feat_row=(self.item_features[j][:]).reshape(1,self.feature_count)
      actual_ratings=(self.data[idy,j]).reshape(len(idy),1)
      #the calculation below is based on formula 1 and formula 3
      error=np.matmul(np.transpose((np.matmul(user_feat_row,np.transpose(item_feat_row)))-actual_ratings),user_feat_row)
      self.item_features[j][:]=self.item_features[j][:]-self.learning_rate*(error+self.lamb*((item_feat_row)))

  def update_uf_mat(self):
    for j in range(0,self.user_count):
       #idx is tuple of all item_ids which have been rated by usr j in current batch
      idx=np.nonzero(self.data[j,:])[0]
      user_feat_row=(self.user_features[j][:]).reshape(1,self.feature_count) #Theta_temp
      item_feat=self.item_features[idx][:]#X_tem
      actual_ratings=(self.data[j][idx]).reshape(1,len(idx))#Y_tem
      #the calculation below is based on formula 2 and formula 3
      error=(np.matmul((np.transpose(np.matmul(item_feat,(np.transpose(user_feat_row))))-actual_ratings),item_feat))
      self.user_features[j][:]=self.user_features[j][:]-self.learning_rate*(error+self.lamb*((self.user_features[j][:]).reshape(1,self.feature_count)))

  def train_model(self):
    for k in range(1,self.epochs+1):
      self.update_if_mat();
      self.update_uf_mat();
      print("Epoch {} completed- RMSE- {}".format(k,self.rmse()))
    return(self.rmse())

  def predict(self,user_id):
    idn=np.where(self.data[user_id,:] == 0)[0]
    if (len(idn)==0):
      product_ids=(-self.predicted_mat[user_id,idn]).argsort()[:20]
      return (product_ids)


if __name__=='__main__':
  A=pd.read_csv("C:/Users/admin/Desktop/BE RS/Utility_Mat_Average.csv")


  A.index=A.iloc[:, 0]
  A.drop(columns=[A.columns[0]],inplace=True)

  A=A.iloc[0:8000,0:700]

  a=A.values
  a=np.floor(a)
  mat_fact = Mat_Factorization(a,feature_count=10,epochs=100,lamb=0.01,learning_rate=0.0001)
  #you can change lambda to 0.1 and also reduce epochs since after a certain point, there isn't much change observed
  rmse=mat_fact.train_model()
  print(rmse)
  pickle.dump(mat_fact, open('model.pkl', 'wb'))
